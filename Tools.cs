﻿
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace QCOM_xml_parse
{
   public static class Tools
   {
      public static bool IsNumber(string strNumber)
      {

         Regex objNotNumberPattern = new Regex("[^0-9.-]");
         Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
         Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
         string strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
         string strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
         Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");

         return !(strNumber == "-") && !objNotNumberPattern.IsMatch(strNumber) &&
             !objTwoDotPattern.IsMatch(strNumber) &&
             !objTwoMinusPattern.IsMatch(strNumber) &&
             objNumberPattern.IsMatch(strNumber);
      }
      public static bool IsContains(List<string> KeyWords, string Text)
      {
         foreach (var Key in KeyWords)
         {
            if (Key.Contains(Text))
            {
               return true;
            }
         }
         return false;
      }
   }

}
